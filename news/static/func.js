$(document).ready(function() {
    $("#submit").on("click", function(e) {
        var q = $("#input").val();
        // console.log(q);
        //var hasil = 'https://www.googleapis.com/books/v1/volumes?q=' + q;
        // var hasil2 = 'https://newsapi.org/v2/top-headlines?q=' + q + '&country=id&apiKey=3568a5b71b3841a8959ac10f2b116f66'
        //console.log(hasil);
        // console.log(hasil2);
        $.ajax({
            url: data_news_url + q,
            success: function(data) {
                console.log(data);
                var hasil_data = $("#card");
                hasil_data.empty();
                for (i = 0; i < data.articles.length; i++) {
                    var x = data.articles[i].title;
                    var y = data.articles[i].urlToImage;
                    var z = data.articles[i].description;
                    var a = data.articles[i].source.name;
                    var b = data.articles[i].url;
                    var c = data.articles[i].likes;
                    hasil_data.append(
                    '<div class="card mb-3"style="max-width: 540px;">' +
                    '<div class="row no-gutters">' + 
                    '<div class="col-md-4">' + 
                    '<img '+'src="' + y + '"' + 'id="cover" style="max-width:150%;padding-right: 120px;margin-top: 100px;margin-left: 25px;">'
                    + '</div>' + '<div class="col-md-8">' + 
                    '<div class="card-body">' + 
                    '<h5 class="card-title" id="title">' + x + '</h5>'
                    + '<p class="card-text" id="desc">Description : '+ z +'</p>' + 
                    '<p class="card-text" id="src">Source : '+ a +'</p>' + 
                    '<a href="' + b +'" target="_blank" id="link">Direct Link to The Article</a>' +
                    '<p><button class="btn btn-warning" onclick="addLike()"> Like The News </button></p>' + 
                    ' <h4> Total Likes : </h4> <h4 id="like"> ' + c + '</h4>');
                }
            }
        });
    });
});

function addLike(){
    console.log("terpanggil ga ya?")
    var z = parseInt(document.getElementById("like").innerHTML) + 1
    document.getElementById("like").innerHTML = z
    var x = document.getElementById("like").innerHTML
    var url = document.getElementById("link").href
    var src = document.getElementById("src").innerHTML
    var desc = document.getElementById("desc").innerHTML
    var cov = document.getElementById("cover").innerHTML
    var title = document.getElementById("title").innerHTML
    const news={
        judul:title,
        cover:cov,
        description:desc,
        source:src,
        link:url,
        likes:x
    }
    console.log(news.url)
    $.ajax({
        type:"POST",
        url:add_like_url,
        data:news

    })
}
