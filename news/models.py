from django.db import models

# Create your models here.
class komentar(models.Model):
    nama = models.CharField(default="Anonymous",max_length=50)
    komentar = models.CharField(blank=False, max_length= 300)

    def __str__(self):
        return self.komentar

class newsArticles(models.Model):
    title = models.CharField(max_length=130, primary_key=True)
    cover = models.CharField(max_length=400)
    description = models.CharField(max_length=500)
    sourceName = models.CharField(max_length=200, blank=True)
    url = models.CharField(max_length=400)
    likes = models.IntegerField(default=0)
