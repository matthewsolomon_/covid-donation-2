from django.shortcuts import render, redirect
from .forms import komentarForm, updateKasusForm
from django.http import JsonResponse
import json
import requests
from .models import komentar
from .models import newsArticles
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
# def news(request):
#     return render(request, 'news.html')

# def jumlahkasus(request):
#     return render(request, 'jumlahkasus.html')

def tambahKomentar(request):
    if request.method == 'POST':
        form = komentarForm(request.POST)
        if form.is_valid():
            inpMasuk = komentar()
            inpMasuk.nama = request.user.username
            inpMasuk.komentar = form.cleaned_data['komentar']
            inpMasuk.save()
        return redirect("/news")

    else:
            inpMasuk = komentar.objects.all()
            form = komentarForm()
            response = {'inpMasuk':inpMasuk, 'form': form}
            return render(request, 'news.html', response)

def kasus(request):
    if request.method == 'POST':
        form2 = updateKasusForm(request.POST)
        return redirect("/detail")
    else:
        form2 = updateKasusForm()
        response = {'form2' : form2}
        return render(request, 'jumlahkasus.html', response)

def search(request):
    q = request.GET['q']
    
    url_tujuan2 = 'https://newsapi.org/v2/top-headlines?q=' + q + '&sortBy=popularity&country=id&category=health&apiKey=961851e1166e40b7bff978b6caa23692'
    
    req = requests.get(url_tujuan2)

    # x = json.loads(req.content)
    x = req.json()

    for item in x['articles']:
        try:
            newsObj = newsArticles.objects.get(title=item['title'])
            likes = newsObj.likes   

        except:
            likes = 0
        item['likes'] = likes

    # return JsonResponse(x, safe=False)
    return JsonResponse(x)

@csrf_exempt
def addLike(request):
    
    newsObj, created = newsArticles.objects.get_or_create(
        # title=request.POST['title'],
        title=request.POST.get('title', 'none'),
        cover=request.POST['cover'],
        description=request.POST['description'],
        # sourceName=request.POST['sourceName'],
        sourceName=request.POST.get('sourceName', 'none'),
        # url=request.POST['url'],
        url=request.POST.get('url', 'none'),
        
    )
    newsObj.likes=int(request.POST['likes'])
    
    newsObj.save()
    return JsonResponse(newsObj.likes, safe=False)
