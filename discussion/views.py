from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from .models import Post, Comment
from .forms import NewCommentForm, PostForm
from django.views.generic import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.defaultfilters import slugify

def index(request):
    all_posts = Post.newmanager.all()
    return render(request, 'index.html', {'posts': all_posts})


def post_single(request, post):

    post = get_object_or_404(Post, slug=post, status='published')

    comments = post.comments.filter(status=True)

    user_comment = None

    if request.method == 'POST':
        comment_form = NewCommentForm(request.POST)
        if comment_form.is_valid():
            user_comment = comment_form.save(commit=False)
            user_comment.post = post
            user_comment.name = request.user.username
            user_comment.save()
            return HttpResponseRedirect('/discussion/' + post.slug)
    else:
        comment_form = NewCommentForm()
    return render(request, 'single.html', {'post': post, 'comments':  user_comment, 'comments': comments, 'comment_form': comment_form, 'user': request.user.username })

def new_post(request):
    form = PostForm(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        post = Post()
        post.author = request.user.username
        post.title = form.cleaned_data['title']
        post.excerpt = form.cleaned_data['excerpt']
        post.content = form.cleaned_data['content']
        post.slug = slugify(post.title)
        post.save()
        return HttpResponseRedirect('/discussion/')
    context = {
        'form': form,
        'user': request.user.username
    }
    return render(request, 'newpost.html', context)