from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class Payment(models.Model):
    nama = models.CharField(blank=False, max_length= 100)
    amount = models.PositiveIntegerField(blank=False, null=True)
    message = models.TextField(blank=True, max_length= 100)
    cardNumber = models.PositiveIntegerField(blank=False, null=True)
    valideDate = models.PositiveIntegerField(blank=False, null=True)
    code = models.IntegerField(blank=False, null=True)
    tanggal = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return self.nama
