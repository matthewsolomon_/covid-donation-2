$(document).ready(function(){
    $("#donasi2").click(function(){
        $("#foto").toggle();
    });

    $("input[type=text]").keyup(function(){  
        $(this).val($(this).val().toUpperCase());  
    });
});

$("#form-d").submit(function(event){
    alert("Terima kasih telah memberi donasi");
    event.preventDefault();
});

$("#mode").click(function() {
    console.log("berhasil");
    $("#canvas-wrapper").toggleClass("dark-mode");
    $(".row-atas").toggleClass("dark-mode");
});


const alertBox = document.getElementById('alert-box')
const form = document.getElementById('form-d')


const nama = document.getElementById('id_nama')
const amount = document.getElementById('id_amount')
const message = document.getElementById('id_message')

const cardNumber = document.getElementById('id_cardNumber')
const valideDate = document.getElementById('id_valideDate')
const code = document.getElementById('id_code')

const csrf = document.getElementsByName('csrfmiddlewaretoken')
console.log(csrf)

const url = ""

const handleAlerts = (type, text) => {
    alertBox.innerHTML = `<div class="alert alert-${type}" role="alert">
                            ${text}
                        </div>`
}
    


form.addEventListener('submit', e=>{
    e.preventDefault()

    const fdata = new FormData()
    fdata.append('csrfmiddlewaretoken', csrf[0].value)
    fdata.append('nama', nama.value)
    fdata.append('amount', amount.value)
    fdata.append('message', message.value)
    fdata.append('cardNumber', cardNumber.value)
    fdata.append('valideDate', valideDate.value)
    fdata.append('code', code.value)

    console.log(fdata)

    $.ajax({
        type: 'POST',
        data: fdata,
        url: url,
        enctype: 'multipart/form-data',
        success: function(response){
            console.log(response)
            const st = `${response.nama} successfully donate`
            // console.log(st)
            handleAlerts('success', st)
            console.log(handleAlerts)
            
            // alert(st)
            setTimeout(() =>{
                alertBox.innerHTML = ""
                nama.value = ""
                amount.value = ""
                message.value = ""
                cardNumber.value = ""
                valideDate.value = ""
                code.value = ""
            }, 1000)

        },
        error: function(xhr, textStatus, error) {
            console.log('response: ' + xhr.responseText);
            console.log('status: ' + xhr.statusText);
            console.log('status: ' + textStatus);
            console.log(error);
        },
        cache: false,
        contentType: false,
        processData: false,
    })
})

// console.log(form)