from django.db import models
from django.contrib.auth.models import User

class Story(models.Model):
    name = models.CharField(max_length=40, blank=False)
    description = models.TextField(max_length=300, blank=False)

