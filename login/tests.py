from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from .views import logging_in, register
from .models import Story
import json

# Login Function Unit Test
class AnimalTestCase(TestCase):
    def setUp(self):
        Story.objects.create(name="watame", description="watame wa warukunaiyo ne")
        Story.objects.create(name="botan", description="la lion")

    def test_description(self):
        botan = Story.objects.get(name="botan")
        watame = Story.objects.get(name="watame")
        self.assertEqual(botan.description, 'la lion')
        self.assertEqual(watame.description, 'watame wa warukunaiyo ne')
    
    def test_name(self):
        botan = Story.objects.get(name="botan")
        watame = Story.objects.get(name="watame")
        self.assertEqual(botan.name, 'botan')
        self.assertEqual(watame.name, 'watame')

class loginFunctionTests(TestCase):
    def test_url_register(self):
        resp = Client().get('/account/register/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_login(self):
        resp = Client().get('/account/logging_in/')
        self.assertEquals(200, resp.status_code)


class TestUrls(TestCase):
    def testLoginURL(self):
        loginF = resolve(reverse("login:logging_in")).func
        self.assertEquals(loginF, logging_in)
    def testRegisterURL(self):
        registerF = resolve(reverse("login:register")).func
        self.assertEquals(registerF, register)


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_url = reverse("login:logging_in")
        self.register_url = reverse("login:register")
        self.logout_url = reverse("login:logging_out")

    def testLoginTemplates(self):
        self.assertEquals(self.client.get(reverse("login:logging_in")).status_code, 200)
        self.assertTemplateUsed(self.client.get(reverse("login:logging_in")), "login/login.html")

    def testRegisterTemplates(self):
        self.assertEquals(self.client.get(reverse("login:register")).status_code, 200)
        self.assertTemplateUsed(self.client.get(reverse("login:register")), "login/register.html")

    def testRegLoginLogout(self):
        repondre1 = self.client.post(self.register_url, data = {
            "username" : "momosuzunene",
            "password1" : "botan123lamy",
            "password2" : "botan123lamy"
        }, follow=True)
        self.assertEquals(repondre1.status_code, 200)
        self.assertTemplateUsed(repondre1, "home/index.html")

        repondre2 = self.client.post(self.logout_url, data = {}, follow = True)
        self.assertContains(repondre2, "LOGIN")
        self.assertContains(repondre2, "SIGN UP")
        self.assertTemplateUsed(repondre2, "home/index.html")

class TestStory(TestCase):
    def test_url_story(self):
        response = self.client.get('/account/story/')
        self.assertEqual(response.status_code, 302)

    def test_ajax_story(self):
        account = User.objects.create_user('watame', 'watame@yahoo.com', 'watame123wa')
        self.client.force_login(account)
        response = self.client.post('/account/story/', {'description' : "watame wa warukunaiyo ne"}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        response = self.client.get('/account/story/')
        repond = response.content.decode('utf8')
        self.assertIn("watame wa warukunaiyo ne", repond)

    def test_get_story(self):
        Story.objects.create(name='watame', description='watame ga suki')
        response = self.client.get('/account/getStory')
        self.assertEqual(response.status_code,404)

class TestProfiles(TestCase):
    def test_url_test(self):
        response = self.client.get('/account/profiles/')
        self.assertEqual(response.status_code, 200)

