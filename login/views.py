from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login , logout, authenticate
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm, AuthenticationForm, StoryForm
from django.http import JsonResponse
from .models import Story
from django.contrib.auth.decorators import login_required
import json

# Login System
def register(request):
    if request.user.is_authenticated:
        return redirect("home:index")
    context = {
        "registering":RegistrationForm
    }

    if request.method == "POST":
        registering = RegistrationForm(request.POST)
        if registering.is_valid():
            registering.save()
            new_user_account = authenticate(
                username = registering.cleaned_data['username'],
                password = registering.cleaned_data['password1']
            )
            '''new_userz = Profs.objects.create(
                user = new_user_account,
                full_name = new_user_account.username
            )'''
            login(request, new_user_account)

            return redirect("home:index")
        elif registering.errors:
            context["error"] = "Input the right data!"
    return render(request, 'login/register.html', context)

def logging_in(request):
    context = {"logging_in": LoginForm}
    if request.user.is_authenticated:
        return redirect("home:index")

    if request.method == "POST":
        logging_in = LoginForm(data = request.POST)
        if logging_in.is_valid():
            user_account = logging_in.get_user()
            login(request, user_account)
            return redirect("home:index")
        elif logging_in.errors:
            context["error"] = "Invalid datas"
    return render(request, 'login/login.html', context)

def logging_out(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("home:index")

def profiles(request):
    context = {
        "profs" : "Anonymous" if not request.user.is_authenticated else request.user.username
    }
    return render(request,"login/profiles.html",context)

@login_required(login_url="/account/logging_in")
def story(request):
    if request.method == 'POST':
        story_form = StoryForm(request.POST)
        if story_form.is_valid():
            print("hai")
            story_new = Story()
            story_new.name = request.user.username
            story_new.description = story_form.cleaned_data['description']
            story_new.save()
    form = StoryForm()
    response = {'form':form}
    response['data'] = Story.objects.all()
    return render(request, 'login/story.html',response)


def getStory(request, name):
    userReq = Story.objects.all().values()
    lst = list(userReq)
    return JsonResponse(lst, safe=False)
