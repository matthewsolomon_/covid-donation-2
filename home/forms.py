from django import forms

class TipsTrickForm(forms.Form):
    # name = forms.CharField(label="Nama", widget=forms.HiddenInput(attrs={
    #     'class': 'form-control',
    #     'type' : 'text',
    #     'placeholder': 'Masukkan Nama Anda',
    #     'required': False,
    # }))

    message = forms.CharField(label="Tips & Trick", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Tulis Tips & Trick versi kamu!',
        'required': True,
        'id':'message_input'
    }))
