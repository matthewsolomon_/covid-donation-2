from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import TipsTrickForm
from .models import TipsTrick
import json
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def penggunaan_masker(request):
    return render(request,'home/penggunaan-masker.html')

@login_required(login_url="/account/logging_in")
def tips_trick(request):
    response = {}
    if request.method == 'POST':
        if request.is_ajax():
            tips_form = TipsTrickForm(request.POST)
            if tips_form.is_valid():
                tips_trick = TipsTrick()
                tips_trick.name = request.user.username
                tips_trick.message = tips_form.cleaned_data['message']
                tips_trick.save()
                return JsonResponse({'success':True})
        return JsonResponse({'success':False})
    else:
        form = TipsTrickForm()
        response = {'form':form}
        response['data'] = TipsTrick.objects.all()
        return render(request, 'home/tips-trick.html',response)


def getTipsTrick(request):
    userReq = TipsTrick.objects.all().values()
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)