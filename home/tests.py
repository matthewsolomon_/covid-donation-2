from django.test import TestCase
from django.contrib.auth.models import User
from .forms import TipsTrickForm
from .models import TipsTrick

import json

# Create your tests here.
class TipsTrickHTMLTest(TestCase):
    def test_text_exist(self):
        response = self.client.get('/tips-trick/')

        self.assertIn('', response.content.decode())
        self.assertIn('', response.content.decode())
        self.assertIn('', response.content.decode())

    def test_submit_button(self):
        response = self.client.get('/tips-trick/')

        self.assertIn('', response.content.decode())

    def test_form(self):
        response = self.client.get('/tips-trick/')
        
        self.assertIn('', response.content.decode())
        self.assertIn('', response.content.decode())

class TipsTrickViewsTest(TestCase):
    def test_form_valid(self):
        response = self.client.post('/tips-trick/', data={
            'name': 'Qonita',
            'message': 'Pake masker',
        })
        self.assertFalse(TipsTrick.objects.filter(name="Qonita").exists())


class HomeUnitTest(TestCase):
    def test_model_function(self):
        tips = TipsTrick.objects.create(name="tips1", message="Content of tips1")
        self.assertIn('tips1', str(tips))
        tipe_dict = tips.get_dict()
        self.assertIn('tips1', tipe_dict['name'])
        self.assertIn('Content of tips1', tipe_dict['message'])

    def test_penggunaan_masker_url(self):
        response = self.client.get('/penggunaan-masker/')
        self.assertEqual(response.status_code, 200)

    def test_tips_trick(self):
        user = User.objects.create_user('qonita', 'test@test.com','testpassword')
        self.client.force_login(user)
        tips = TipsTrick.objects.create(name="tips1", message="Content of tips1")
        response = self.client.get('/tips-trick/')
        self.assertIn('tips1', response.content.decode('utf8'))
        self.assertIn('Content of tips1', response.content.decode('utf8'))

    def test_tips_trick_post_ajax(self):
        user = User.objects.create_user('qonita', 'test@test.com', 'testpassword')
        self.client.force_login(user)
        response = self.client.post('/tips-trick/', {
                    'message' : "Content of tips1"
                },
                HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        response = self.client.get('/tips-trick/')
        self.assertIn('tips1', response.content.decode('utf8'))
        self.assertIn('Content of tips1', response.content.decode('utf8'))

    def test_tips_trick_post_not_ajax(self):
        user = User.objects.create_user('qonita', 'test@test.com', 'testpassword')
        self.client.force_login(user)
        response = self.client.post('/tips-trick/', {
                    'message' : "Content of tips1"
        })

        self.assertFalse(json.loads(response.content.decode('utf8'))['success'])

    def test_get_tips_trick(self):
        TipsTrick.objects.create(name='tips1', message='Content of tips1')
        response = self.client.get('/getTipsTrick/')
        self.assertEqual(200, response.status_code)